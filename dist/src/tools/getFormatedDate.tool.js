"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(date) {
    if (date === void 0) { date = new Date(); }
    return date.toLocaleDateString() + ' ' + date.toLocaleTimeString();
}
exports.default = default_1;
