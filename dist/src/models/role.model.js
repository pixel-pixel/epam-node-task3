"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var roleShema = new mongoose_1.Schema({
    value: { type: String, required: true, default: 'USER' }
});
exports.default = (0, mongoose_1.model)('Role', roleShema);
