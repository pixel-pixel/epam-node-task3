"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var joi_1 = __importDefault(require("joi"));
var addLoadForUserSchema = joi_1.default.object({
    name: joi_1.default.string().required(),
    payload: joi_1.default.number().required(),
    pickup_address: joi_1.default.string().required(),
    delivery_address: joi_1.default.string().required(),
    dimensions: joi_1.default.object({
        width: joi_1.default.number().required(),
        length: joi_1.default.number().required(),
        height: joi_1.default.number().required()
    }).required()
});
exports.default = addLoadForUserSchema;
