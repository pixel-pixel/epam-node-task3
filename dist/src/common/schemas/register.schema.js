"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var joi_1 = __importDefault(require("joi"));
var Roles_enum_1 = __importDefault(require("../enums/Roles.enum"));
var registerSchema = joi_1.default.object({
    email: joi_1.default.string().email().required(),
    password: joi_1.default.string().min(6).required(),
    role: joi_1.default.string().valid(Roles_enum_1.default.Driver, Roles_enum_1.default.Shipper).required()
});
exports.default = registerSchema;
