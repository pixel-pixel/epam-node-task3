"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoadStatus;
(function (LoadStatus) {
    LoadStatus["NEW"] = "NEW";
    LoadStatus["POSTED"] = "POSTED";
    LoadStatus["ASSIGNED"] = "ASSIGNED";
    LoadStatus["SHIPPED"] = "SHIPPED";
})(LoadStatus || (LoadStatus = {}));
exports.default = LoadStatus;
