"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoadState;
(function (LoadState) {
    LoadState["ROUTE_TO_PICK_UP"] = "En route to Pick Up";
    LoadState["ARIVED_TO_PICK_UP"] = "Arrived to Pick Up";
    LoadState["ROUTE_TO_DELIVERY"] = "En route to delivery";
    LoadState["ARIVED_TO_DELIVERY"] = "Arrived to delivery";
})(LoadState || (LoadState = {}));
exports.default = LoadState;
