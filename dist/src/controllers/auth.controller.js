"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var User_model_1 = __importDefault(require("../models/User.model"));
var role_model_1 = __importDefault(require("../models/role.model"));
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var joi_1 = __importDefault(require("joi"));
var register_schema_1 = __importDefault(require("../common/schemas/register.schema"));
var login_schema_1 = __importDefault(require("../common/schemas/login.schema"));
var token_tool_1 = __importDefault(require("../tools/token.tool"));
var AuthController = /** @class */ (function () {
    function AuthController() {
    }
    AuthController.prototype.register = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var body, email, password, value, user, role, hashPassword, newUser, e_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        body = joi_1.default.attempt(req.body, register_schema_1.default, 'invalid method`s body');
                        email = body.email, password = body.password, value = body.role;
                        return [4 /*yield*/, User_model_1.default.findOne({ email: email })];
                    case 1:
                        user = _a.sent();
                        return [4 /*yield*/, role_model_1.default.findOne({ value: value })];
                    case 2:
                        role = _a.sent();
                        if (user)
                            throw Error('user with this email already exist');
                        if (!role)
                            throw Error('invalid role');
                        hashPassword = bcryptjs_1.default.hashSync(password, 7);
                        newUser = new User_model_1.default({
                            email: email,
                            password: hashPassword,
                            role: value
                        });
                        return [4 /*yield*/, newUser.save()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, res.json({ message: "Profile created successfully" })];
                    case 4:
                        e_1 = _a.sent();
                        message = e_1.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    AuthController.prototype.login = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var body, email, password, user, validPassword, jwt_token, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        body = joi_1.default.attempt(req.body, login_schema_1.default);
                        email = body.email, password = body.password;
                        return [4 /*yield*/, User_model_1.default.findOne({ email: email })];
                    case 1:
                        user = _a.sent();
                        if (!user)
                            throw Error('user with this email don`t exist');
                        validPassword = bcryptjs_1.default.compareSync(password, user.password);
                        if (!validPassword)
                            throw Error('bad password');
                        jwt_token = (0, token_tool_1.default)(user._id, user.role);
                        res.json({ jwt_token: jwt_token });
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        message = e_2.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return AuthController;
}());
exports.default = new AuthController;
