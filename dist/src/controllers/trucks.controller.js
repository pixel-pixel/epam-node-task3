"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var joi_1 = __importDefault(require("joi"));
var Truck_model_1 = __importDefault(require("../models/Truck.model"));
var createEditTruck_schema_1 = __importDefault(require("../common/schemas/createEditTruck.schema"));
var getFormatedDate_tool_1 = __importDefault(require("../tools/getFormatedDate.tool"));
var TrucksController = /** @class */ (function () {
    function TrucksController() {
    }
    TrucksController.prototype.getUsersTrucks = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var _id, trucks, e_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        _id = req['user']._id;
                        return [4 /*yield*/, Truck_model_1.default.find({ created_by: _id })];
                    case 1:
                        trucks = _a.sent();
                        return [2 /*return*/, res.json({ trucks: trucks })];
                    case 2:
                        e_1 = _a.sent();
                        message = e_1.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    TrucksController.prototype.addUsersTruck = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, _a, type, _b, status_1, truck, e_2, message;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 2, , 3]);
                        userId = req['user']._id;
                        _a = joi_1.default.attempt(req.body, createEditTruck_schema_1.default), type = _a.type, _b = _a.status, status_1 = _b === void 0 ? 'IS' : _b;
                        truck = new Truck_model_1.default({
                            created_by: userId,
                            type: type,
                            status: status_1,
                            created_date: (0, getFormatedDate_tool_1.default)()
                        });
                        return [4 /*yield*/, truck.save()];
                    case 1:
                        _c.sent();
                        return [2 /*return*/, res.json({ message: 'Truck created successfully' })];
                    case 2:
                        e_2 = _c.sent();
                        message = e_2.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    TrucksController.prototype.getUsersTruckById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var _id, truck, e_3, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        _id = req['user']._id;
                        return [4 /*yield*/, Truck_model_1.default.findById(req.params.id)];
                    case 1:
                        truck = _a.sent();
                        if (truck.created_by != _id)
                            throw Error('you don`t have permissions');
                        return [2 /*return*/, res.json({ truck: truck })];
                    case 2:
                        e_3 = _a.sent();
                        message = e_3.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    TrucksController.prototype.editUsersTruckById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, truckId, _a, type, _b, status_2, truck, e_4, message;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 3, , 4]);
                        userId = req['user']._id;
                        truckId = req.params.id;
                        _a = joi_1.default.attempt(req.body, createEditTruck_schema_1.default), type = _a.type, _b = _a.status, status_2 = _b === void 0 ? 'OL' : _b;
                        return [4 /*yield*/, Truck_model_1.default.findById(truckId)];
                    case 1:
                        truck = _c.sent();
                        if (truck.created_by != userId)
                            throw Error('you don`t have permissions');
                        truck.type = type;
                        truck.status = status_2;
                        return [4 /*yield*/, truck.save()];
                    case 2:
                        _c.sent();
                        return [2 /*return*/, res.json({ message: 'Truck assigned successfully' })];
                    case 3:
                        e_4 = _c.sent();
                        message = e_4.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    TrucksController.prototype.deleteUsersTruckById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, trackId, truck, e_5, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        userId = req['user']._id;
                        trackId = req.params.id;
                        return [4 /*yield*/, Truck_model_1.default.findById(trackId)];
                    case 1:
                        truck = _a.sent();
                        if (truck.created_by != userId)
                            throw Error('you don`t have permissions');
                        return [4 /*yield*/, truck.delete()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, res.json({ message: 'Truck deleted successfully' })];
                    case 3:
                        e_5 = _a.sent();
                        message = e_5.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    TrucksController.prototype.asignTruckToUserById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, trackId, assignedTrucks, truck, e_6, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        userId = req['user']._id;
                        trackId = req.params.id;
                        return [4 /*yield*/, Truck_model_1.default.find({ assigned_to: userId })];
                    case 1:
                        assignedTrucks = _a.sent();
                        if (assignedTrucks.length > 0)
                            throw Error('you already assigned to another truck');
                        return [4 /*yield*/, Truck_model_1.default.findById(trackId)];
                    case 2:
                        truck = _a.sent();
                        truck.assigned_to = userId;
                        return [4 /*yield*/, truck.save()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/, res.json({ message: 'Truck assigned successfully' })];
                    case 4:
                        e_6 = _a.sent();
                        message = e_6.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    return TrucksController;
}());
exports.default = new TrucksController;
