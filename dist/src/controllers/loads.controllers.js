"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var joi_1 = __importDefault(require("joi"));
var LoadStatus_enum_1 = __importDefault(require("../common/enums/LoadStatus.enum"));
var TruckStatus_enum_1 = __importDefault(require("../common/enums/TruckStatus.enum"));
var Truck_model_1 = __importDefault(require("../models/Truck.model"));
var Roles_enum_1 = __importDefault(require("../common/enums/Roles.enum"));
var addLoadForUser_shema_1 = __importDefault(require("../common/schemas/addLoadForUser.shema"));
var Load_model_1 = __importDefault(require("../models/Load.model"));
var TruckTypes_enum_1 = require("../common/enums/TruckTypes.enum");
var getFormatedDate_tool_1 = __importDefault(require("../tools/getFormatedDate.tool"));
var LoadState_enum_1 = __importDefault(require("../common/enums/LoadState.enum"));
var LoadsController = /** @class */ (function () {
    function LoadsController() {
    }
    LoadsController.prototype.getUserLoads = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, userId, userRole, loads, truck, loads, e_1, message;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, , 7]);
                        _a = req['user'], userId = _a._id, userRole = _a.role;
                        if (!(userRole == Roles_enum_1.default.Shipper)) return [3 /*break*/, 2];
                        return [4 /*yield*/, Load_model_1.default.find({ created_by: userId })];
                    case 1:
                        loads = _b.sent();
                        res.json({ loads: loads });
                        return [3 /*break*/, 5];
                    case 2: return [4 /*yield*/, Truck_model_1.default.findOne({ assigned_to: userId })];
                    case 3:
                        truck = _b.sent();
                        if (!truck || truck.status === TruckStatus_enum_1.default.IS)
                            return [2 /*return*/, res.json({ loads: [] })];
                        return [4 /*yield*/, Load_model_1.default.find({ assigned_to: truck._id })];
                    case 4:
                        loads = _b.sent();
                        return [2 /*return*/, res.json({ loads: loads })];
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_1 = _b.sent();
                        message = e_1.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.addLoadForUser = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var body, userId, load, e_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        body = joi_1.default.attempt(req.body, addLoadForUser_shema_1.default);
                        userId = req['user']._id;
                        load = new Load_model_1.default(__assign(__assign({}, body), { created_by: userId, created_date: (0, getFormatedDate_tool_1.default)() }));
                        return [4 /*yield*/, load.save()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/, res.json({ message: 'Load created successfully' })];
                    case 2:
                        e_2 = _a.sent();
                        message = e_2.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.getUserActiveLoad = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, truck, load, e_3, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        userId = req['user']._id;
                        return [4 /*yield*/, Truck_model_1.default.findOne({ assigned_to: userId })];
                    case 1:
                        truck = _a.sent();
                        if (!truck || truck.status == TruckStatus_enum_1.default.IS)
                            return [2 /*return*/, res.json({ load: null })];
                        return [4 /*yield*/, Load_model_1.default.findOne({ assigned_to: truck._id })];
                    case 2:
                        load = _a.sent();
                        return [2 /*return*/, res.json({ load: load })];
                    case 3:
                        e_3 = _a.sent();
                        message = e_3.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.nextLoadState = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, truck, load, _a, e_4, message;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 9, , 10]);
                        userId = req['user']._id;
                        return [4 /*yield*/, Truck_model_1.default.findOne({ assigned_to: userId })];
                    case 1:
                        truck = _b.sent();
                        if (!truck)
                            throw Error('you don`t assign to truck');
                        return [4 /*yield*/, Load_model_1.default.findOne({ assigned_to: truck._id })];
                    case 2:
                        load = _b.sent();
                        if (!load)
                            throw Error('truck don`t assign to load');
                        _a = load.state;
                        switch (_a) {
                            case LoadState_enum_1.default.ROUTE_TO_PICK_UP: return [3 /*break*/, 3];
                            case LoadState_enum_1.default.ARIVED_TO_PICK_UP: return [3 /*break*/, 4];
                        }
                        return [3 /*break*/, 5];
                    case 3:
                        load.state = LoadState_enum_1.default.ARIVED_TO_PICK_UP;
                        return [3 /*break*/, 7];
                    case 4:
                        load.state = LoadState_enum_1.default.ROUTE_TO_DELIVERY;
                        return [3 /*break*/, 7];
                    case 5:
                        load.state = LoadState_enum_1.default.ARIVED_TO_DELIVERY;
                        load.status = LoadStatus_enum_1.default.SHIPPED;
                        truck.status = TruckStatus_enum_1.default.IS;
                        return [4 /*yield*/, truck.save()];
                    case 6:
                        _b.sent();
                        _b.label = 7;
                    case 7: return [4 /*yield*/, load.save()];
                    case 8:
                        _b.sent();
                        return [2 /*return*/, res.json({ message: "Load state changed to '" + load.state + "'" })];
                    case 9:
                        e_4 = _b.sent();
                        message = e_4.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.getUserLoadById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, loadId, load, truckId, e_5, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        userId = req['user']._id;
                        loadId = req.params.id;
                        return [4 /*yield*/, Load_model_1.default.findById(loadId)];
                    case 1:
                        load = _a.sent();
                        return [4 /*yield*/, Truck_model_1.default.findOne({ assigned_to: userId })];
                    case 2:
                        truckId = (_a.sent())._id;
                        console.log(userId + ' ' + load.assigned_to + ' ' + load.created_by);
                        if (load.assigned_to != truckId && load.created_by != userId) {
                            throw Error('you don`t have permissions');
                        }
                        return [2 /*return*/, res.json({ load: load })];
                    case 3:
                        e_5 = _a.sent();
                        message = e_5.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.updateUserLoadById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var body, userId, loadId, load, e_6, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        body = joi_1.default.attempt(req.body, addLoadForUser_shema_1.default);
                        userId = req['user']._id;
                        loadId = req.params.id;
                        return [4 /*yield*/, Load_model_1.default.findById(loadId)];
                    case 1:
                        load = _a.sent();
                        if (load.created_by != userId)
                            throw Error('you don`t have permissin');
                        load = Object.assign(load, body);
                        load.save();
                        return [2 /*return*/, res.json({ message: 'Load details changed successfully' })];
                    case 2:
                        e_6 = _a.sent();
                        message = e_6.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.deleteUserLoadById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, loadId, load, e_7, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        userId = req['user']._id;
                        loadId = req.params.id;
                        return [4 /*yield*/, Load_model_1.default.findById(loadId)];
                    case 1:
                        load = _a.sent();
                        if (load.created_by != userId)
                            throw Error('you don`t have permissions');
                        return [4 /*yield*/, load.delete()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, res.json({ message: 'Load deleted successfully' })];
                    case 3:
                        e_7 = _a.sent();
                        message = e_7.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.postUserLoadById = function (req, res) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var userId, loadId, load_1, trucks, findedTruck, e_8, message;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _c.trys.push([0, 9, , 10]);
                        userId = req['user']._id;
                        loadId = req.params.id;
                        return [4 /*yield*/, Load_model_1.default.findById(loadId)];
                    case 1:
                        load_1 = _c.sent();
                        if (load_1.created_by != userId)
                            throw Error('you don`t have permissions');
                        load_1.status = LoadStatus_enum_1.default.POSTED;
                        return [4 /*yield*/, load_1.save()];
                    case 2:
                        _c.sent();
                        return [4 /*yield*/, Truck_model_1.default.find()
                                .where('status').equals(TruckStatus_enum_1.default.IS)
                                .where('assigned_to').ne(null)];
                    case 3:
                        trucks = _c.sent();
                        findedTruck = trucks.find(function (t) {
                            var sizes = (0, TruckTypes_enum_1.getSizes)(t.type);
                            return sizes.payload >= load_1.payload &&
                                sizes.width >= load_1.dimensions.width &&
                                sizes.length >= load_1.dimensions.length &&
                                sizes.height >= load_1.dimensions.height;
                        });
                        if (!findedTruck) return [3 /*break*/, 6];
                        findedTruck.status = TruckStatus_enum_1.default.OL;
                        return [4 /*yield*/, findedTruck.save()];
                    case 4:
                        _c.sent();
                        load_1.status = LoadStatus_enum_1.default.ASSIGNED;
                        load_1.assigned_to = findedTruck._id;
                        load_1.state = LoadState_enum_1.default.ROUTE_TO_PICK_UP;
                        load_1.logs = (_a = load_1.logs) !== null && _a !== void 0 ? _a : [];
                        load_1.logs.push({
                            message: "truck found, id: " + findedTruck._id,
                            time: (0, getFormatedDate_tool_1.default)()
                        });
                        return [4 /*yield*/, load_1.save()];
                    case 5:
                        _c.sent();
                        return [2 /*return*/, res.json({
                                message: 'Load posted successfully',
                                driver_found: true
                            })];
                    case 6:
                        load_1.logs = (_b = load_1.logs) !== null && _b !== void 0 ? _b : [];
                        load_1.logs.push({
                            message: 'truck not found',
                            time: (0, getFormatedDate_tool_1.default)()
                        });
                        return [4 /*yield*/, load_1.save()];
                    case 7:
                        _c.sent();
                        return [2 /*return*/, res.json({
                                message: 'Load can`t be posted. Driver not found',
                                driver_found: false
                            })];
                    case 8: return [3 /*break*/, 10];
                    case 9:
                        e_8 = _c.sent();
                        message = e_8.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 10];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    LoadsController.prototype.getUserLoadShippingInfoById = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userId, loadId, load, truck, e_9, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        userId = req['user']._id;
                        loadId = req.params.id;
                        return [4 /*yield*/, Load_model_1.default.findById(loadId)];
                    case 1:
                        load = _a.sent();
                        if (load.created_by != userId)
                            throw Error('you don`t have permission');
                        return [4 /*yield*/, Truck_model_1.default.findById(load.assigned_to)];
                    case 2:
                        truck = _a.sent();
                        return [2 /*return*/, res.json({ load: load, truck: truck })];
                    case 3:
                        e_9 = _a.sent();
                        message = e_9.message;
                        res.status(400).json({ message: message });
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    return LoadsController;
}());
exports.default = new LoadsController;
