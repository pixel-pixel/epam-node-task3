"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Roles_enum_1 = __importDefault(require("../common/enums/Roles.enum"));
var loads_controllers_1 = __importDefault(require("../controllers/loads.controllers"));
var auth_middleware_1 = __importDefault(require("../middlewares/auth.middleware"));
var loadsRouter = (0, express_1.Router)();
loadsRouter.get('/', loads_controllers_1.default.getUserLoads);
loadsRouter.post('/', (0, auth_middleware_1.default)(Roles_enum_1.default.Shipper), loads_controllers_1.default.addLoadForUser);
loadsRouter.get('/active', (0, auth_middleware_1.default)(Roles_enum_1.default.Driver), loads_controllers_1.default.getUserActiveLoad);
loadsRouter.patch('/active/state', (0, auth_middleware_1.default)(Roles_enum_1.default.Driver), loads_controllers_1.default.nextLoadState);
loadsRouter.get('/:id', loads_controllers_1.default.getUserLoadById);
loadsRouter.put('/:id', loads_controllers_1.default.updateUserLoadById);
loadsRouter.delete('/:id', (0, auth_middleware_1.default)(Roles_enum_1.default.Shipper), loads_controllers_1.default.deleteUserLoadById);
loadsRouter.post('/:id/post', (0, auth_middleware_1.default)(Roles_enum_1.default.Shipper), loads_controllers_1.default.postUserLoadById);
loadsRouter.get('/:id/shipping_info', (0, auth_middleware_1.default)(Roles_enum_1.default.Shipper), loads_controllers_1.default.getUserLoadShippingInfoById);
exports.default = loadsRouter;
