"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var trucks_controller_1 = __importDefault(require("../controllers/trucks.controller"));
var trucksRouter = (0, express_1.Router)();
trucksRouter.get('/', trucks_controller_1.default.getUsersTrucks);
trucksRouter.post('/', trucks_controller_1.default.addUsersTruck);
trucksRouter.get('/:id', trucks_controller_1.default.getUsersTruckById);
trucksRouter.put('/:id', trucks_controller_1.default.editUsersTruckById);
trucksRouter.delete('/:id', trucks_controller_1.default.deleteUsersTruckById);
trucksRouter.post('/:id/assign', trucks_controller_1.default.asignTruckToUserById);
exports.default = trucksRouter;
